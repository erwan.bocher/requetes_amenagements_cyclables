-- Les bandes cyclables comptées une fois (et qui ne sont pas des doubles-sens cyclables) sont :

(	-- Soit qui ne sont que d'un coté d'une route à double sens et sont à sens uniques
	(	-- voies à double sens
		"oneway" IS NULL
		OR
		"oneway" NOT IN ('yes','-1')
	)
	AND
	(	--bande cyclable d'un seul côté
		(	-- seulement à droite
			"cycleway:right"='lane'
			AND
			(	-- pas à gauche
				"cycleway:left" IS NULL
				OR
				"cycleway:left" NOT IN ('lane')
			)
			AND
			(	-- et pas à double sens
				"cycleway:right:oneway" IS NULL
				OR
				"cycleway:right:oneway" NOT LIKE 'no'
			)
		)
	)
)

OR
(	-- Soit qui ne sont que du coté de circulation des routes à sens unique 
	(	-- route à sens unique (avec yes)
		(
			"oneway"='yes'
			OR
			"junction" IN ('roundabout','circular')
		)
		AND
		(
			(--avec une bande cyclable
				"cycleway"='lane'
					OR
				"cycleway:right"='lane'
			)
			AND
			( --et pas a double sens 
				"cycleway:right:oneway" NOT LIKE 'no'
				OR
				"cycleway:right:oneway" IS NULL
			)
				
		)
	)
	OR
	(	-- route à sens unique (avec -1)
		"oneway"='-1'
		AND
		(
			"cycleway:right"='lane' --avec une bande cyclable (obligatoirement left sinon il s'agit d'un double-sens cyclable)
			AND
			(	-- et pas à double sens
				"cycleway:right:oneway" IS NULL
				OR
				"cycleway:right:oneway" NOT LIKE 'no'
			)
			AND
			(	--et pas en en DSC
				"oneway:bicycle" NOT LIKE 'no'
				OR
				"oneway:bicycle" IS NULL
			)
		)
	)
)
