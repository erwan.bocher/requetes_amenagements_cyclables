(	-- Soit
	(	-- une route à double sens
		(
			"oneway" IS NULL
			OR
			"oneway" LIKE 'no'
		)
		AND
		(
			"junction" IS NULL
			OR
			"junction" NOT IN ('roundabout','circular')
		)
	)
	AND --et qui a
	(	-- soit une bande cyclable classique
		"cycleway" = 'shared_lane'
		OR
		(	-- soit une bande cyclable à droite et à gauche quelque soit le sens
			"cycleway:right" = 'shared_lane'
			AND "cycleway:left" = 'shared_lane'
		)
		OR	-- soit une bande cyclable à droite et à gauche quelque soit le sens
		"cycleway:both" = 'shared_lane'
	)
)

