-- Double-sens cyclables avec bandes (compter une fois)

-- Cas pour les opposite_lane

(	-- Une route à sens unique
	"oneway" IN ('yes','-1')
	AND
	(	-- qui a un DSC expicitement à droite
		"cycleway:right" LIKE 'opposite_lane'
	)		
)
OR
(	-- Une route à sens unique taguée avec oneway=-1
	"oneway" LIKE '-1'
	AND
	(
		"cycleway" = 'opposite_lane'
		OR
		(	-- qui a une bande cyclable à droite implicitement à contre sens du fait du -1
			"cycleway:right" LIKE 'lane'
			AND
			"oneway:bicycle" LIKE 'no'
			AND
			(	--et qui n'est pas à double sens 
				"cycleway:right:oneway" IS NULL
				OR
				"cycleway:right:oneway" NOT LIKE 'no'
			)	
		)
		OR
		(	-- qui a une bande cyclable à droite et à gauche et donc implicitement à contre sens
			"cycleway:both" LIKE 'lane'
			AND
			"oneway:bicycle" LIKE 'no'
		)
	)
)
