(
	( "oneway" IS NULL OR "oneway" NOT IN ('yes','-1'))
	AND
	( "oneway:bicycle" IS NULL OR "oneway" NOT IN ('yes','-1'))
)
AND
(
	( --cas des footway tolérées aux vélo (permissive) on ne prend pas les trottoirs
		( "highway" = 'footway' AND ( "footway" IS NULL OR "footway" NOT IN ('sidewalk')))
		AND
		"bicycle" = 'permissive'
	)
	OR
	( --cas des path tolérées aux vélo (permissive)
		"highway" = 'path'
		AND
		"bicycle" = 'permissive'
	)
)
AND
(   -- Elles ont un revetement circulable au VTC vélo de randonnée ou le revetement n'est pas connu
    (
        (
            "surface" IN ('paved','asphalt','concrete','concrete:plates','concrete:lanes','paving_stones','sett','unhewn_cobblestone','cobblestone','metal','wood','unpaved','compacted','fine_gravel','gravel','pebblestone','ground','tartan','clay','metal_grid' )
            OR
            "surface" IS NULL
        )
        AND
        (
            "smoothness" NOT IN ('bad','very_bad','horrible','very_horrible','impassable')
            OR
            "smoothness" IS NULL
        )
        AND
        (
            "tracktype" NOT iN ('grade3','grade4','grade5')
            OR
            "tracktype" IS NULL
        )
    )
)