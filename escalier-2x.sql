
-- Sont comptabilisés uniquement les escaliers ayant un aménagement pour les vélos

"highway" = 'steps' -- un escalier

AND

(
    "ramp:bicycle" = 'yes' --avec une goulotte pour les vélos sans précision de coté
    OR
    "ramp:bicycle:both" = 'yes'
    OR
    (  --avec une goulotte pour les vélos de chaque coté
        "ramp:bicycle:right" = 'yes'
        AND
        "ramp:bicycle:left" = 'yes'
    )
)