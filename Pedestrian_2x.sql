-- Seul le sens voiture est pris en compte

"highway" LIKE 'pedestrian'
AND
(
	"oneway" IS NULL
	OR
	"oneway" LIKE 'no'
)
AND
(
	"bicycle" IS NULL
	OR
	"bicycle" NOT IN ('no')
)
