--A double sens 
(
"oneway" IS NULL
OR
"oneway" LIKE 'no'
)

AND
	--soit une voie de type living street
	(
	"highway" LIKE 'living_street'

	OR
	
	--soit une voie avec le tag zone:maxspeed ou source:maxspeed
		( 
		"maxspeed"='20'
		AND
			(
			"zone:maxspeed"='FR:20'
			OR
			"source:maxspeed"='FR:zone20'
			)
		)
	)
	

