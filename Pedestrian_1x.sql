-- Seul le sens voiture est pris en compte

"highway" IN ('pedestrian')
AND
"oneway" IN ('yes','-1')
AND
(
	"bicycle" IS NULL
	OR
	"bicycle" NOT IN ('no')
)
