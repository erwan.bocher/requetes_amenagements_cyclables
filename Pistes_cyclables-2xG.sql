-- Une piste cyclable à double sens d'un seul côté de la route (:left/right), rattachée à une route (track) qui peut être à sens unique ou pas :
	(	-- la piste peut être à gauche
		"cycleway:left" IN ('track','opposite_track')
		AND
		"cycleway:left:oneway" LIKE 'no'
	)
