--Soit
	(	-- une route à sens unique ou non, quand la bande de gauche est à double sens
		"cycleway:left" IN ('lane','opposite_lane')
		AND
		"cycleway:left:oneway" LIKE 'no'
	)
