
(
    "cyclestreet" = 'yes'
    OR
    "bicycle_road" = 'yes'
)
AND
(
	"oneway" IS NULL
	OR
	"oneway" LIKE 'no'
)
AND
(
	"bicycle" IS NULL
	OR
	"bicycle" NOT IN ('no')
)