("segregated" IS NULL OR "segregated" NOT IN  ('yes'))
AND
"highway" LIKE 'footway'
AND
"footway" LIKE 'sidewalk'
AND
"bicycle" NOT LIKE 'no'
AND
(
	"oneway" IN ('yes','-1')
	OR
	"oneway:bicycle" IN ('yes','-1')
)
