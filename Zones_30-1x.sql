-- Sont comptabilisés uniquement les zones 30 et pas les voies limitees à 30
-- Seul le sens voiture est pris en compte

"maxspeed" LIKE '30'
AND
(
	"zone:maxspeed" LIKE 'FR:30'
	OR
	"source:maxspeed" LIKE 'FR:zone30'
)
AND "oneway" IN ('yes','-1')
AND
(
	"bicycle" IS NULL
	OR
	"bicycle" NOT IN ('no')
)
